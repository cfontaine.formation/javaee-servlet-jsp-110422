<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- inclure une autre jsp en passant des paramètres --%>
<jsp:include page="header.jsp">
    <jsp:param value="Element d'action" name="titre"/>
</jsp:include>

<h3>élément d'action</h3>

<%-- Java bean --%>
<%-- déclaration  du bean -> useBean --%>
<jsp:useBean id="u1" class="fr.dawan.projetjavaee.beans.User" scope="page"/>
<%-- Modifier les attributs du bean -> setProperty---%>
<jsp:setProperty name="u1" property="prenom" value="John"/>
<jsp:setProperty name="u1" property="nom" value="DOE"/>

<%-- Obtenir les attributs du bean -> getProperty---%>
<div class="alert alert-primary col-md-6">le prénom : <jsp:getProperty name="u1" property="prenom"/>
,le nom : <jsp:getProperty name="u1" property="nom"/></div>

<%@include file="footer.jsp"%>