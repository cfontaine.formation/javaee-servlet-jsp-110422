package fr.dawan.projetjavaee.controllers;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.dawan.projetjavaee.beans.User;

/**
 * Servlet implementation class TestTagList
 */
@WebServlet("/taglib")
public class ExempleTagLib extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExempleTagLib() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String str="<div class='alert alert-danger'>test</div>";
	    User u=new User();
	    double[] t= {1.2,3.4,4.5,6.7};
	    request.setAttribute("i", 42);
	    request.setAttribute("str", str);
	    request.setAttribute("user",u);
	    request.setAttribute("t",t);
	    request.setAttribute("date", new Date());
	    request.getRequestDispatcher("WEB-INF/taglib.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
