<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="header.jsp">
    <c:param name="titre" value="Envoie de mail"/>
</c:import>
<h2 class="mb-4"> Envoie de mail</h2>
<div class="my-2 col-md-6" >
<form method="post" action='<c:url value="/email" context="/projetjavaee" />' >
    <div class="mb-3">
    <label class="form-label" for="dest">Destinataire</label>
    <input class="form-control" type="email" name="dest" id="dest" placeholder="Entrer le destinataire"/>
    </div>
    <div class="mb-3">
    <label class="form-label" for="dest">Expediteur</label>
    <input class="form-control" type="email" name="exp" id="exp" placeholder="Entrer le expéditeur"/>
    </div>
    <div class="mb-3">
    <label class="form-label" for="titre">Titre</label>
    <input class="form-control" type="text" name="titre" id="titre" placeholder="Entrer le titre"/>
    </div>
    <div class="mb-3">
    <label class="form-label" for="content">Message</label>
    <textarea class="form-control" rows="5" cols="50" name="content" id="content">
    </textarea>
    </div>
    <input class="btn btn-primary"  type="submit" value="Envoyer"/>
</form>
</div>

<c:import url="footer.jsp"/>