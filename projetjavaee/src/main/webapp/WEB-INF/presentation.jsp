<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>

<%@ include file="header.jsp" %>
<h1>Exercice Présentation MVC</h1>
<a href="presentation?name=Alan Smithee">Salutation</a>
<a href="presentation">J'ignore</a>

<% String name=(String)request.getAttribute("name");
   if(name!=null && !name.isEmpty()){%>
   <div class= "alert alert-primary col-md-3">Bonjour, <%=name%></div>
<% } %>   
   
<hr>

<form method="post" action="presentation">
    <div class="mb-3">
    <label class="col-md-2 form-label" for="prenom">Prénom</label>
    <input class="form-control" "type="text" name="prenom" id="prenom" placeholder="Entrer votre prénom"/>
    </div>
    
    <div class="mb-3">
    <label class="col-md-2 form-label" for="nom">Nom</label>
    <input class="form-control" "type="text" name="nom" id="nom" placeholder="Entrer votre nom"/>
    </div>
    <input class="btn btn-primary mb-3" type="submit" value="confirmer">
</form>
<%String msg= (String)request.getAttribute("msg");
  if(msg!=null && !msg.isEmpty()){
      String alertColor=(Boolean) request.getAttribute("err")?"alert-danger":"alert-primary";
%>

 <div class="alert <%= alertColor%> col-md-3"><%=msg %></div>
<%} %>
<%@ include file="footer.jsp" %>
