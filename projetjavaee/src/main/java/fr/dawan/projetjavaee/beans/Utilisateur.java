package fr.dawan.projetjavaee.beans;

public class Utilisateur extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    private String prenom;
    
    private String nom;
    
    private String email;
    
    private String password;

    public Utilisateur() {
    }

    public Utilisateur(String prenom, String nom, String email, String password) {
        this.prenom = prenom;
        this.nom = nom;
        this.email = email;
        this.password = password;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Utilisateur [prenom=" + prenom + ", nom=" + nom + ", email=" + email + ", password=" + password
                + ", " + super.toString() + "]";
    }
}
