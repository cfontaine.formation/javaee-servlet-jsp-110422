package fr.dawan.projetjavaee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.projetjavaee.beans.Utilisateur;

public class UtilisateurDao extends GenericDao<Utilisateur> {

    @Override
    protected void create(Utilisateur entity, Connection connection) throws SQLException {
        PreparedStatement ps=connection.prepareStatement("INSERT INTO utilisateurs(prenom,nom,email,password) VALUES (?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
        ps.setString(1,entity.getPrenom());
        ps.setString(2,entity.getNom());
        ps.setString(3,entity.getEmail());
        ps.setString(4,entity.getPassword());
        ps.executeUpdate();
        ResultSet rs= ps.getGeneratedKeys();
        if(rs.next()) {
            entity.setId(rs.getLong(1));
        }
    }

    @Override
    protected void update(Utilisateur entity, Connection connection) throws SQLException {
        PreparedStatement ps=connection.prepareStatement("UPDATE utilisateurs SET prenom=?, nom=?, email=?,password=? WHERE id=?");
        ps.setString(1,entity.getPrenom());
        ps.setString(2,entity.getNom());
        ps.setString(3,entity.getEmail());
        ps.setString(4,entity.getPassword());
        ps.setLong(5,entity.getId());
        ps.executeUpdate();
        
    }

    @Override
    protected void remove(long id, Connection connection) throws SQLException {
        PreparedStatement ps=connection.prepareStatement("DELETE FROM utilisateurs WHERE id=?");
        ps.setLong(1, id);
        ps.executeUpdate();
    }

    @Override
    protected Utilisateur findById(long id, Connection connection) throws SQLException {
        Utilisateur u=null;
        PreparedStatement ps=connection.prepareStatement("SELECT prenom,nom,email,password FROM utilisateurs WHERE id=?");
        ps.setLong(1, id);
       ResultSet rs= ps.executeQuery();
       if(rs.next()) {
           u=new Utilisateur(rs.getString("prenom"),rs.getString("nom"),rs.getString("email"),rs.getString("password"));
           u.setId(id);
       }
       return u;
    }

    @Override
    protected List<Utilisateur> findAll(Connection connection) throws SQLException {
        List<Utilisateur> lst=new ArrayList<>();
        PreparedStatement ps=connection.prepareStatement("SELECT id,prenom,nom,email,password FROM utilisateurs");
       ResultSet rs= ps.executeQuery();
       while(rs.next()) {
           Utilisateur u=new Utilisateur(rs.getString("prenom"),rs.getString("nom"),rs.getString("email"),rs.getString("password"));
           u.setId(rs.getLong("id"));
           lst.add(u);
       }
       return lst;
    }
    
    // Pour trouver un utilisateur en focntion de son email
    public Utilisateur findByMail(String email, boolean close) {
        Utilisateur u=null;
        Connection cnx=getConnection();
        try {
            PreparedStatement ps=cnx.prepareStatement("SELECT id,prenom,nom,password FROM utilisateurs WHERE email=?");
            ps.setString(1, email);
            ResultSet rs=ps.executeQuery();
            if(rs.next()) {
                u=new Utilisateur(rs.getString("prenom"),rs.getString("nom"),email,rs.getString("password"));
                u.setId(rs.getLong("id")); 
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return u;
    }

}
