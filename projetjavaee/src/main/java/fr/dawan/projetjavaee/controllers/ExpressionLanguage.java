package fr.dawan.projetjavaee.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.dawan.projetjavaee.beans.User;

/**
 * Servlet implementation class Expxpresio
 */
@WebServlet("/el")
public class ExpressionLanguage extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExpressionLanguage() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String str = "Test expression language";
        request.setAttribute("test", str);

        User u1 = new User("John", "Doe");
        User u2 = null;
        request.setAttribute("user1", u1);
        request.setAttribute("user2", u2);

        int[] tab = { 1, 2, 4, 8 };
        request.setAttribute("t", tab);

        List<String> lst = new ArrayList<>();
        lst.add("azerty");
        lst.add("uiop");
        lst.add("dfghjk");
        request.setAttribute("lst", lst);

        Map<String, Integer> m = new HashMap<>();
        m.put("v1", 34);
        m.put("v2", 12);
        request.setAttribute("m", m);

        request.getRequestDispatcher("WEB-INF/el.jsp").forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}
