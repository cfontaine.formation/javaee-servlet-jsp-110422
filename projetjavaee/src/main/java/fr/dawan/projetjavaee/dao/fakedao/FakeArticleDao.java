package fr.dawan.projetjavaee.dao.fakedao;

import java.util.ArrayList;
import java.util.List;

import fr.dawan.projetjavaee.beans.Article;

// Permet de simuler l'accès à une base de donnée en attendant l'écriture de la partie persistence des objets
public class FakeArticleDao {
    // L'accès à la base est simulé par un liste static qui est initialisé par un
    // bloc statique
    private static List<Article> lst = new ArrayList<>();

    // Un bloc static est éxécuté une seule fois lors du premier objet instancier
    // avant l'éxécution du constructeur
    static {
        Article a = new Article("Tv 4k", 500.0, "Télé 60 pouce");
        a.setId(1);
        lst.add(a);
        a = new Article("Smartphone", 200.0, "Smartphone android");
        a.setId(2);
        lst.add(a);
        a = new Article("Souris", 20.0, "");
        a.setId(3);
        lst.add(a);
        a = new Article("Clavier", 36.0, "Clavier Gaming Azerty");
        a.setId(4);
        lst.add(a);
    }

    public void saveOrUpdate(Article a) {
        if (a.getId() == 0) {
            create(a);
        } else {
            update(a);
        }
    }

    public void delete(Article a) {
        lst.remove(a);
    }

    public void deleteById(long id) {
        for (Article ar : lst) {
            if (ar.getId() == id) {
                lst.remove(ar);
                break;
            }
        }
    }

    public List<Article> findAll() {
        return lst;
    }

    public Article findById(long id) {
        for (Article ar : lst) {
            if (ar.getId() == id) {
                return ar;
            }
        }
        return null;
    }

    private void create(Article a) {
        lst.add(a);
        a.setId(lst.size() + 1);
    }

    private void update(Article a) {
        for (Article ar : lst) {
            if (ar.getId() == a.getId()) {
                ar.setNom(a.getNom());
                ar.setPrix(a.getPrix());
                ar.setDescription(a.getDescription());
                break;
            }
        }
    }
}
