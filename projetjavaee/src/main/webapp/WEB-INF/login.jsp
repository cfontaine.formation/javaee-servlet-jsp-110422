<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="header.jsp">
    <c:param name="titre" value=" Login"/>
</c:import>


<form method="post" action='<c:url value="/login" context="/projetjavaee"/>'>
<h2 class="my-5 text-center" >Authentification</h2>
    <div class="my-5 col-md-4 offset-md-4">
        <label for="email" class="form-label">Email</label>
        <input type="email" class="form-control" name="email" id="email" value="${email}"/>
    </div>
    <div class="mb-4 col-md-4 offset-md-4">
        <label for="password" class="form-label">Mot de passe</label>
        <input type="password" class="form-control" name="password" id="password"/>
    </div>
    <div class="my-5">
        <input class="btn btn-primary  col-md-4 offset-md-4 my-3" type="submit" value="Confirmer"/>
    </div>    
</form>

<c:if test="${!empty msg && !empty email}">
    <div class="alert alert-danger my-5 col-md-4 offset-md-4"><c:out value="${msg}"/></div>
</c:if>

<c:import url="footer.jsp"/>