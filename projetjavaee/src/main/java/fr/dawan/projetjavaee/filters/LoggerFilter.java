package fr.dawan.projetjavaee.filters;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import fr.dawan.projetjavaee.beans.Utilisateur;

/**
 * Servlet Filter implementation class LoggerFilter
 */
@WebFilter("/*")
public class LoggerFilter extends HttpFilter implements Filter {
       
    private static Logger LOGGER= Logger.getLogger(LoggerFilter.class.getName());
    
    /**
     * @see HttpFilter#HttpFilter()
     */
    public LoggerFilter() {
        super();
        // TODO Auto-generated constructor stub
    }

    static {
        try {
            FileHandler fileHandler =new FileHandler("projetjavaee.log");
            fileHandler.setFormatter(new SimpleFormatter());
            LOGGER.addHandler(fileHandler);
        } catch (Exception e) {
                  e.printStackTrace();
        }
    }
    
	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		String url=((HttpServletRequest)request ).getServletPath();
		HttpSession session=((HttpServletRequest)request).getSession();
        Utilisateur u=(Utilisateur)session.getAttribute("user");
        LOGGER.info(u!=null?url +" "+u.getEmail():url );
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
