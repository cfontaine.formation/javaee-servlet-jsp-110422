package fr.dawan.projetjavaee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.projetjavaee.beans.Article;

public class ArticleDao extends GenericDao<Article> {

    @Override
    protected void create(Article entity, Connection connection) throws SQLException {
        // Statement.RETURN_GENERATED_KEY -> pour récupérer l'id généré par la base de
        // donnée
        PreparedStatement ps = connection.prepareStatement("INSERT INTO articles(nom,prix,description) VALUES (?,?,?)",
                Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, entity.getNom());
        ps.setDouble(2, entity.getPrix());
        ps.setString(3, entity.getDescription());
        ps.executeUpdate();
        // Récupération de l'id générer par la base de donnée
        ResultSet rs = ps.getGeneratedKeys();
        if (rs.next()) {
            entity.setId(rs.getLong(1));
        }

    }

    @Override
    protected void update(Article entity, Connection connection) throws SQLException {
        PreparedStatement ps = connection
                .prepareStatement("UPDATE articles SET nom=?, prix=?,description=? WHERE id=?");
        ps.setString(1, entity.getNom());
        ps.setDouble(2, entity.getPrix());
        ps.setString(3, entity.getDescription());
        ps.setLong(4, entity.getId());
        ps.executeUpdate();
    }

    @Override
    protected void remove(long id, Connection connection) throws SQLException {
        PreparedStatement ps = connection.prepareStatement("DELETE FROM article WHERE id=?");
        ps.setLong(1, id);
        ps.executeUpdate();
    }

    @Override
    protected Article findById(long id, Connection connection) throws SQLException {
        Article article = null;
        PreparedStatement ps = connection.prepareStatement("SELECT nom,prix,description FROM articles WHERE id=?");
        ps.setLong(1, id);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            article = new Article(rs.getString("nom"), rs.getDouble("prix"), rs.getString("description"));
            article.setId(id);
        }
        return article;
    }

    @Override
    protected List<Article> findAll(Connection connection) throws SQLException {
        List<Article> lst = new ArrayList<>();
        PreparedStatement ps = connection.prepareStatement("SELECT id,nom,prix,description FROM articles");
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Article article = new Article(rs.getString("nom"), rs.getDouble("prix"), rs.getString("description"));
            article.setId(rs.getLong("id"));
            lst.add(article);
        }
        return lst;
    }

    
    // On peut ajouter d'autre méthode au DAO ex: pour trouver l'article le moins chère,pour retourner le nombre d'article ...
    // ...
}
