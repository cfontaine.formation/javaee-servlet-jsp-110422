package fr.dawan.projetjavaee.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FirstServlet
 */
// Déclaration de la servlet en utilisant l'annotation @WebServlet à partir des servlet 3.0
// name -> nom de la servlet
// urlPatterns -> une url ou plusieurs url associées à la servlet
// loadOnStartup -> chargement de la servlet au démarrage du serveur, la valeur correspond à l'ordre de démarrage
// initParams -> paramètres d'initilisation de la servlet, on définit un paramètre avec l'annotation @WebInitParam 
@WebServlet(name = "firstservlet", urlPatterns = { "/firstservlet", "/fs",
        "/premiereservlet" }, loadOnStartup = 1, initParams = { @WebInitParam(name = "annee", value = "2021"),
                @WebInitParam(name = "testparam", value = "test") })
public class FirstServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public FirstServlet() {
        super();
        System.out.println("Constucteur first servlet");
    }

    /**
     * @see Servlet#init(ServletConfig)
     * Méthode appelée par le contenneur de servlet après l'instanciation de la servlet 
     */
    public void init(ServletConfig config) throws ServletException {
        System.out.println("Méthode init");
        // récupération des paramètres d'initialisation de la servlet définie à sa déclaration
        System.out.println("annee=" + config.getInitParameter("annee"));
        System.out.println("testparam=" + config.getInitParameter("testparam"));
        super.init(config); // Appel de la méthode init de la classe mère => sinon  getServletConfig retourne null
    }

    /**
     * @see Servlet#destroy()
     * Méthode appelée par le contenneur de servlet juste avant la destruction de la servlet
     */
    public void destroy() {
        System.out.println("Méthode destroy");
    }

    /**
     * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
     *      response)
     * La méthode service est appelée par le conteneur de servlet dés qu'une requète à même url que celle associé à la servlet
     *  HttpServletRequest request => objet qui correspond à la requète HTTP
     *  HttpServletResponse responset => objet qui correspond à la réponse HTTP qui sera envoyée au client
     */
    protected void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("Méthode service");
        super.service(request, response);   // La méthode service de HttpServlet va appeler 
    }                                       // la méthode doXXX correspondant au type de la requête HTTP

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     *  Méthode appelé par service si la requête est de "type" Get
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        System.out.println("Méthode doGet");
        
        // getServletConfig -> permet d'accéder au paramètre d'initialisation en dehors de la méthode init
        System.out.println(getServletConfig().getInitParameter("annee"));
        
        // Requête
        // Affichage dans la console de tous le paramètres de la requête nom et valeur associée
        Enumeration<String> paramNames = request.getParameterNames(); // request.getParameterNames() retourne une énumération de tous les paramètres de la requête
        while (paramNames.hasMoreElements()) {
            String name = paramNames.nextElement();
            System.out.println(name + " = " + request.getParameter(name)); // request.getParameter -> retourne la valeur du paramètre passé à la méthode
        }
        
        // En-tête
        // affiche dans la console Les nom/valeur contenu dans l'en-tête de la requête
        System.out.println(request.getContentType());               // type MIME du contenu de la requête
        System.out.println(request.getCharacterEncoding());         // encodage du corps de la requête
        System.out.println(request.getLocale().getDisplayLanguage()); // langage par défaut du client
        Enumeration<Locale> locales = request.getLocales();         // tous les langages préférés du clients ordonnés par préférences
        while (locales.hasMoreElements()) {
            Locale l = locales.nextElement();
            System.out.println(l.getDisplayLanguage());
        }

        // URL
        // Affiche des informations contenue dans l'URL
        System.out.println(request.getServerName());    // Nom du serveur
        System.out.println(request.getServerPort());    // Port du serveur    
        System.out.println(request.getContextPath());   // contextPath -> nom du projet 
        System.out.println(request.getServletPath());   // url de la servlet
        System.out.println(request.getRequestURL());    // URL
        System.out.println(request.getRemoteAddr());    // obtenir l’adresse IP du client
        System.out.println(request.getRemotePort());    // obtenir le port du client
        System.out.println(request.getProtocol());      // protocole
        System.out.println(request.isSecure());         // HTTPS -> true
        
        // Réponse
        // Fixer la valeur du statut de la réponse
        response.setStatus(202);
        // response.setStatus(418);
        // response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

        // Signaler une erreur ou un problème dans le traitement de la requête
        // response.sendError(404,"Ressource non trouvée");
        

        // En-têtes
        response.setContentType("text/html"); // définir l'en-tête Content-Type pour indiquer le type MIME de la
                                              // ressource
        response.setCharacterEncoding("utf-8");
        // response.setContentType("text/html;charset=utf-8"); // ou
        response.setLocale(Locale.FRENCH); // définir l'en-tête Content-Language =fr

        // response.setHeader("Content-Language", "fr-CA"); // ajouter une en-tête, si elle existe déjà, elle est écrasée
        // response.addHeader("Content-Language", "es-ES"); // ajouter une en-tête, mais si elle existe déjà, la valeur lui est ajoutée

        // Redirection -> 302 rediriger de la requête vers une URL
        // response.sendRedirect("https://www.google.fr");
        
        // Corps de la requête
        // getWriter() d'obtenir le flux de sortie texte de la servlet (caractères)
        // pour écrire du texte dans la réponse HTTP
        PrintWriter pw = response.getWriter();
        pw.println("<html>");
        pw.println("<head><title>Première servlet</title></head>");
        pw.println("<body>");
        pw.println("<h1>Première servlet:" + LocalDate.now().toString() + "</h1>");
        pw.println("<h3>Paramètres d'initalisation de la servlet</h3>");
        pw.println("<ul>");
        Enumeration<String> paramInitNames=getServletConfig().getInitParameterNames();
        while(paramInitNames.hasMoreElements()) {
            String name=paramInitNames.nextElement();
            pw.print("<li>" + name + " = ");
            pw.print(getServletConfig().getInitParameter(name));
            pw.print("</li>");
        }
        
        pw.println("</ul>");
        pw.println("<h3>Paramètres de la requète</h3>");
        pw.println("<ul>");
        Map<String, String[]> mParam = request.getParameterMap();
        for (Entry<String, String[]> param : mParam.entrySet()) {
            pw.print("<li> " + param.getKey() + " = ");
            for (String val : param.getValue()) {
                pw.print(val + " ");
            }
            pw.print("</li>");
        }
        pw.println("</ul>");
        
        pw.println("<h3>En-tête de la requête</h3>");
        pw.println("<ul>");
        Enumeration<String> namesHeader=request.getHeaderNames();
        while(namesHeader.hasMoreElements()) {
            String name=namesHeader.nextElement();
            Enumeration<String> values=request.getHeaders(name);
            pw.print("<li> " + name + " = ");
            while(values.hasMoreElements()) {
                String v=values.nextElement();
                pw.print(v + " ");
            }
            pw.println("</li>");
        }
        pw.println("</ul></body>");
        pw.println("</html>");

    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     * Méthode appelé par service si la requéte est de "type" POST
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("Méthode doPost");
        doGet(request, response);
    }

    /**
     * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
     * Méthode appelé par service si la requéte est de "type" PUT
     */
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("Méthode doPut");
    }

}
