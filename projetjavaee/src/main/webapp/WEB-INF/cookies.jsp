<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="header.jsp">
    <c:param name="titre" value="Cookie"/>
</c:import>
<h2 class="mb-4">Liste des cookies</h2>
<table class="table">
    <thead>
        <tr>
        <th>Nom</th>
        <th>Valeur</th>
        </tr>
    </thead>
    <tbody>
        
        <c:forEach var="c" items="${cook}">
        <tr>
            <td><c:out value="${c.name}"/></td>
             <td><c:out value="${c.value}"/></td>
        </tr>
        </c:forEach>
    
    </tbody>
</table>

<form class="mt-5 col-md-8" action="cookie" method="post" >
    <div class="mb-2">
        <label for="name" class="form-label">Nom</label>
        <input class="form-control" type="text" name="name" id=name/>
    </div>
    <div class="mb-2">
        <label for="value" class="form-label">Valeur</label>
        <input class="form-control" type="text" name="value" id=value/>
    </div>
   <div class="mb-4">
        <label for="duree" class="form-label">Durée</label>
        <input class="form-control" type="Number" name="duree" id=duree/>
    </div>
    <div>
        <input class=" btn btn-primary col-md-2" type="submit" value="Confirmer"/>
    </div>
</form>

<c:import url="footer.jsp"/>