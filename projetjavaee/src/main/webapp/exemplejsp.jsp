<%-- Directive Page --%>
<%-- language --%>
<%@page language="java" 
        contentType="text/html; charset=UTF-8" 
        pageEncoding="UTF-8" 
        import="java.time.LocalDateTime"%> <%-- Import de LocalDateTime --%>

<%-- Directive Include --%>
<%-- Permet d'inclure le fichier header.jsp dans index.jsp --%>
<%@ include file="/WEB-INF/header.jsp" %>

<h2>Exemple JSP</h2>
<!-- Commentaire HTML -->
<%-- Commentaire JSP (n'apparait pas dans la réponse--%>

<%-- Scriplet --%>
<h4>Scriplet &lt;% .. %&gt;</h4>
<p> 
<% String str="Hello";
out.println(str);   // out -> objet implicite pour ecrire dans le flux de sortie text de la servlet
out.println(val);
%>
</p>


<%-- Evaluation d'une expression --%>
<h4>Evaluation d'une expression &lt;%= .. %&gt;</h4>
<div class="alert alert-primary col-md-4">
<%= bonjour %>
</div>
<p>
<%= val*10 %>
</p>


<%-- Objet implicite --%>
<h4>Objets implicites </h4>
<%--request objet qui permet d'accéder à la requête HTTP --%>
<p>
<% out.println(request.getHeader("user-agent"));%>
</p>
<%--request objet qui permet d'accéder à la réponse HTTP --%>
<% response.setStatus(418); %>

<%-- Accès au contexte d’application --%>
<p><%= application.getInitParameter("configFile") %></p>
<p><%= application.getAttribute("global") %></p>


<%-- Déclaration de variable (une variable peut-être utilisée avant d'être déclarée)--%>
<h4>Déclaration de variable &lt;%! .. %&gt; </h4>
<%! int val=42;
    String bonjour="Bonjour";
%>
<p>
<% LocalDateTime hj=LocalDateTime.now();
out.println(hj.toString());
%>
</p>

<%@ include file="/WEB-INF/footer.jsp" %>