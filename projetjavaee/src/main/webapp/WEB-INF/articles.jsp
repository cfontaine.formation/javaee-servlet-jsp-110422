<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="header.jsp">
    <c:param name="titre" value="Gestion Articles"/>
</c:import>
<h3 class="mb-3">Gestion des articles</h3>
<a class="btn btn-sm btn-primary my-2" href='<c:url value="/articles?action=add" context="/projetjavaee" />'>Ajouter Article</a>
<table class="table table-hover">
    <thead>
    <tr>
        <th>Id</th>
        <th>Nom</th>
        <th>Prix</th>
        <th>Description</th>
        <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="article" items="${articles}">
            <c:url var="urlRemove" value="/articles" context="/projetjavaee">
                <c:param name="action" value="remove"/>
                <c:param name="id" value="${article.id}"/>
            </c:url>
            <c:url var="urlUpdate" value="/articles" context="/projetjavaee">
                <c:param name="action" value="update"/>
                <c:param name="id" value="${article.id}"/>
            </c:url>
            
            <tr>
                <td><c:out value="${article.id}"/></td>
                <td><c:out value="${article.nom}"/></td>
                <td><c:out value="${article.prix}"/></td>
                <td><c:out value="${article.description}"/></td>
                <td>
                <a class="btn btn-danger mr-2 btn-sm" href="${urlRemove}">Supprimer</a>
                <a class="btn btn-success btn-sm" href="${urlUpdate}">Modifier</a>
                </td>
            </tr>
        
        </c:forEach>
    
    
    </tbody>

</table>

<c:if test="${!empty msgErr}">
    <div class="alert alert-danger col-md-10 offset-md-1"><c:out value="${msgErr}"/></div>
</c:if>

<c:import url="footer.jsp"/>