<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%-- Bootstrap 5 CSS --%>
<link rel="stylesheet" href="webjars/bootstrap/5.1.3/css/bootstrap.min.css">
<title>

<%--<% String titre=(String)request.getParameter("titre");
out.print(titre!=null?titre:"Formation JavaEE");

ou --%>

${empty param.titre? "Formation JavaEE": param.titre}
</title>
</head>
<body>
<nav class="navbar navbar-expand-lg sticky-top navbar-dark bg-dark mb-4">
<a class="navbar-brand mx-2" href="/projetjavaee/">Formation javaEE</a>
    <button class="navbar-toggler" type="button" data-bs-toogle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
<div class="container">
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="navbar-nav me-auto">
	    
	       <%-- bootstrap -> menu déroulant dans la navbar --%>
	       <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownServlet" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Servlet
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownServlet">
                    <li><a class="dropdown-item" href="/projetjavaee/fs?param=value">First Servlet</a></li>
                    <li><a class="dropdown-item" href="/projetjavaee/affimg">Serlvet flux binaire</a></li>
                </ul>
            </li>
        
            <%-- bootstrap -> élément de la navbar --%>
            <li class="nav-item">
                <a class="nav-link" href="/projetjavaee/exemplejsp.jsp">JSP</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPresentation" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Présentation 
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownPresentation">
                    <li><a class="dropdown-item" href="/projetjavaee/presentation.jsp">Présentation jsp</a></li>
                    <li><a class="dropdown-item" href="/projetjavaee/presentation">Présentation mvc</a></li>
                </ul>
            </li>
        
           <li class="nav-item">
                <a class="nav-link" href="/projetjavaee/mvc?name=john%20doe">MVC</a>
            </li>

	       <li class="nav-item">
                <a class="nav-link" href="/projetjavaee/elmaction">Elément d'action</a>
            </li>

	       <li class="nav-item">
                <a class="nav-link" href="/projetjavaee/el">EL</a>
            </li>
            
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownTaglib" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    TagLib 
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownTaglib">
                    <li><a class="dropdown-item" href="/projetjavaee/taglib">Exemple Taglib</a></li>
                    <li><a class="dropdown-item" href="/projetjavaee/customtag">TagLib personnailsée</a></li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/projetjavaee/articles">Gestion articles</a>
            </li>
           <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownErreur" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Erreur
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownErreur">
                    <li><a class="dropdown-item" href="/projetjavaee/generror?choix=jsp">jsp page directive</a></li>
                    <li><a class="dropdown-item" href="/projetjavaee/existepas">Erreur 404</a></li>
                      <li><a class="dropdown-item" href="/projetjavaee/generror?choix=418">Erreur 418</a></li>
                    <li><a class="dropdown-item" href="/projetjavaee/generror?choix=500">Erreur 500</a></li>
                    <li><a class="dropdown-item" href="/projetjavaee/generror?choix=exception">Exception</a></li>

                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/projetjavaee/cookie">Cookies</a>
            </li>
             <li class="nav-item">
                <a class="nav-link" href="/projetjavaee/session">Session</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/projetjavaee/email">Email</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/projetjavaee/upload">Upload</a>
            </li>
        </ul>
        <c:choose>
            <%-- sessionScope -> accès à un objet stocker dans la session --%>
            <c:when test="${empty sessionScope.user}"> 
                <a class="btn btn-success btn-sm" href='<c:url value="/login" context="/projetjavaee"/>'>Login</a>
            </c:when>
            <c:otherwise>
             <a class="btn btn-light btn-sm" href='<c:url value="/login?logout=true" context="/projetjavaee"/>'>Logout, ${sessionScope.user.prenom}</a>
            </c:otherwise>
        </c:choose>
	</div>
</div>
</nav>
<main class="container-fluid">