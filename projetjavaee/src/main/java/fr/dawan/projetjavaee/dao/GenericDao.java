package fr.dawan.projetjavaee.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import fr.dawan.projetjavaee.beans.AbstractEntity;

// Classe abstraite générique pour créer un DAO 
public abstract class GenericDao<T extends AbstractEntity> {

    private Connection cnx = null;

    private Properties cnxProperties;

    public GenericDao() {
//        try (FileInputStream fis = new FileInputStream("db.properties")) {
//            cnxProperties = new Properties();
//            cnxProperties.load(fis);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    public void SaveOrUpdate(T entity, boolean close) {
        try {
            if (entity.getId() == 0) {
                create(entity, getConnection());
            } else {
                update(entity, getConnection());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(close);
        }
    }

    public void remove(T entity, boolean close) {
        remove(entity.getId(), close);
    }

    public void remove(long id, boolean close) {
        try {
            remove(id, getConnection());
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(close);
        }
    }

    public T findById(long id, boolean close) {
        T e = null;
        try {
            e = findById(id, getConnection());
        } catch (SQLException e1) {
            e1.printStackTrace();
        } finally {
            closeConnection(close);
        }
        return e;
    }

    public List<T> findAll(boolean close) {
        List<T> lst = new ArrayList<>();
        try {
            lst = findAll(getConnection());
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(close);
        }

        return lst;
    }

    protected abstract void create(T entity, Connection connection) throws SQLException;

    protected abstract void update(T entity, Connection connection) throws SQLException;

    protected abstract void remove(long id, Connection connection) throws SQLException;

    protected abstract T findById(long id, Connection connection) throws SQLException;

    protected abstract List<T> findAll(Connection connection) throws SQLException;

    protected Connection getConnection() {
        if (cnx == null) {
            try {
                // Class.forName(cnxProperties.getProperty("driver"));
                // cnx = DriverManager.getConnection(cnxProperties.getProperty("url"),
                // cnxProperties.getProperty("userDb"),
                // cnxProperties.getProperty("passwordDb"));
                Class.forName("org.mariadb.jdbc.Driver");
                cnx = DriverManager.getConnection("jdbc:mariadb://localhost:3306/formation", "root", "dawan");
                // cnxProperties.getProperty("passwordDb"));
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return cnx;
    }

    protected void closeConnection(boolean close) {
        if (close && cnx != null) {
            try {
                cnx.close();
                cnx = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
