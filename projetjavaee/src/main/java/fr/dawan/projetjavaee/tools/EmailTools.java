package fr.dawan.projetjavaee.tools;

import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class EmailTools {
    
    private Properties prop;

    public EmailTools(Properties prop) {
        this.prop = prop;
    }
    
    public void sendHtmlEmail(String expediteur, String destinataire, String titre , String contenu) throws AddressException, MessagingException {
        Session session=Session.getDefaultInstance(prop);
        MimeMessage msg=new MimeMessage(session);
        msg.setFrom(new InternetAddress(expediteur));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress(destinataire));
        msg.setSubject(titre);
        msg.setContent(contenu, "text/html; charset=utf-8");
        msg.setSentDate(new Date());
        Transport.send(msg);
    }
    
    public void sendEmailPj(String destinataire, String expediteur, String sujet, String contenu,String pathFile) throws AddressException, MessagingException {
        Session session=Session.getDefaultInstance(prop);
        MimeBodyPart part1=new MimeBodyPart();
        part1.setText(contenu);
        
        MimeBodyPart part2=new MimeBodyPart();
        FileDataSource fds=new FileDataSource(pathFile);
        part2.setDataHandler(new DataHandler(fds));
        part2.setFileName(fds.getName());
        
        Multipart mp= new MimeMultipart();
        mp.addBodyPart(part1);
        mp.addBodyPart(part2);
        
        MimeMessage msg=new MimeMessage(session);
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress(destinataire));
        msg.setContent(mp);
        msg.setSentDate(new Date());
        Transport.send(msg);
    }


}
