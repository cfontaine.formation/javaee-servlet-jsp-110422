package fr.dawan.projetjavaee.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Presentation
 */
@WebServlet("/presentation")
public class Presentation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Presentation() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter("name");
		request.setAttribute("name", name);
		request.getRequestDispatcher("WEB-INF//presentation.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String prenom=request.getParameter("prenom");
		String nom=request.getParameter("nom");
		String msg="";
		boolean err=true;
		if(prenom!=null && !prenom.isEmpty() && nom!=null && !nom.isEmpty()) {
		    msg= "Bonjour," + prenom +" " + nom;
		    err=false;
		}else {
		    if(prenom==null || prenom.isEmpty()) {
		        msg= "Le prénom ";
		    }
		    if(nom==null || nom.isEmpty()) {
                msg+= msg.length()==0?"Le nom ": "et le nom";
            }
		    msg+=" est vide";
		}
		request.setAttribute("msg", msg);
		request.setAttribute("err", err);
		doGet(request, response);
	}

}
