package fr.dawan.projetjavaee.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TestMvc
 */
@WebServlet("/mvc")
public class ExempleMvc extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExempleMvc() {
        super();
    }

    // Modèle MVC
    // On accède jamais au jsp directement mais par l'intermédiaire d'un servlet
    // La servlet (controlleur) fait le traitement et ensuite on chaine avec l'appel de la jsp (vue)
    // On transmet les données à la vue par l'intermédiare des attributs de la requête
    // Pour empécher l'accés au jsp depuis l'extérieur, on les place dans le dossier WEB-INF 
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter("name");
		String msg;
		if( name!=null && !name.isEmpty()) {
		    msg="name="+name;
		}else
		{
		    msg="Le paramètre name est vide";
		}
		// passer des données à la vue par l'intermédiare des attibuts
		request.setAttribute("msg", msg);
		// chainnage vers la jsp
		request.getRequestDispatcher("WEB-INF//mvc.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}

}
