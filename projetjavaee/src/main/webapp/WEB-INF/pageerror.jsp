<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isErrorPage="true"%>
<%@ include file="header.jsp"%>
<h1>Page Erreur</h1>
<div class="alert alert-danger"><c:out value="${pageContext.exception}"/></div>
<%@ include file="footer.jsp"%>