<%-- Directive Page --%>
<%-- language --%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%-- Directive Include --%>
<%-- Permet d'inclure le fichier header.jsp dans index.jsp --%>
<%@ include file="/WEB-INF/header.jsp"%>
<div class="col-md-6 offset-md-3">
<h2 class="my-4">Formation Java Servlet et JSP</h2>
<ol class=" list-group list-group-flush list-group-numbered ">
	<li class="list-group-item">Servlet</li>
	<li class="list-group-item">JSP</li>
	<li class="list-group-item">Scriplet</li>
	<li class="list-group-item">Element d'action</li>
	<li class="list-group-item">Modéle MVC</li>
	<li class="list-group-item">Cookie</li>
	<li class="list-group-item">Session</li>
	<li class="list-group-item">Context d'application</li>
	<li class="list-group-item">Gestion des erreurs</li>
	<li class="list-group-item">Expression langage</li>
	<li class="list-group-item">TagLib: JSTL (core et fmt), personnalisé</li>
	<li class="list-group-item">Email</li>
	<li class="list-group-item">Upload</li>
</ol>
</div>
<%@ include file="/WEB-INF/footer.jsp"%>