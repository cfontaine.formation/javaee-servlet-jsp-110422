package fr.dawan.projetjavaee.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TestError
 */
@WebServlet("/erreur")
public class Erreur extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Erreur() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Throwable thr = (Throwable) request.getAttribute("javax.servlet.error.exception");
        if (thr == null) {
            int codeError = (Integer) request.getAttribute("javax.servlet.error.status_code");
            String msg;
            switch (codeError) {
            case 404:
                msg = "La page n'existe pas";
                break;
            case 500:
                msg = "Erreur Interne";
                break;
            default:
                msg = "Une erreur c'est produite";
            }
            request.setAttribute("msgError", msg);
            request.setAttribute("codeError", codeError);
            request.getRequestDispatcher("WEB-INF/erreur.jsp").forward(request, response);
        } else {
            request.setAttribute("msg", thr.getMessage());
            request.setAttribute("trace", thr.getStackTrace());
            request.setAttribute("exception", thr.getClass().getName());
            request.getRequestDispatcher("WEB-INF/exception.jsp").forward(request, response);
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }

}
