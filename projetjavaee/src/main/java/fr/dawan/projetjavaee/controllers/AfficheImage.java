package fr.dawan.projetjavaee.controllers;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * AfficheImage => servlet qui retourne un corp de réponse HTTP avec des données binaires image
 */
@WebServlet("/affimg")
public class AfficheImage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AfficheImage() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Obtenir le chemin du dossier WEB-INF
	    String webRootPath=Thread.currentThread().getContextClassLoader().getResource("../").getPath();
	    File fs=new File(webRootPath+"logo.png");
	    if(fs.exists()) {
	        //En-tête reponse
	        response.setContentType("image/png");
	        //Corps reponse => flux binaire
	        BufferedInputStream imgInput=new BufferedInputStream(new FileInputStream(fs));
	        // getOutputStream -> permet d'obtenir le flux binaire de la servlet
	        // pour écrire des données binaires dans la réponse HTTP
	        ServletOutputStream imgOutput=response.getOutputStream();
	        byte[] imgData=new byte[1024];
	        while(imgInput.read(imgData)>0) {
	            imgOutput.write(imgData);
	        }
	        imgInput.close();
	    }else {
	        // En-tête reponse
	        response.setContentType("text/hmtl");
	        response.setCharacterEncoding("utf-8");
	        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
	        // Corps reponse
	        response.getWriter().println("<html>");
	        response.getWriter().println("<head><title>Erreur Affichage Image</title></head>");
	        response.getWriter().println("<body><h1>L'image n'existe pas</h1></body>");
	        response.getWriter().println("</html>");
	    }
	    
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
