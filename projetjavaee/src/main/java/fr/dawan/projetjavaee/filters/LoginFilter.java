package fr.dawan.projetjavaee.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.dawan.projetjavaee.beans.Utilisateur;

/**
 * Servlet Filter implementation class LoginFilter
 */
@WebFilter("/articles/*")
public class LoginFilter extends HttpFilter implements Filter {

    /**
     * @see HttpFilter#HttpFilter()
     */
    public LoginFilter() {
        super();
    }

    /**
     * @see Filter#destroy()
     */
    public void destroy() {
    }

    /**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpSession session=((HttpServletRequest)request).getSession();
		Utilisateur u=(Utilisateur)session.getAttribute("user");
		if(u!=null) {
		    chain.doFilter(request, response);
		}   
		else{
		 ((HttpServletResponse)response).sendRedirect("login");
		}
	}
	
    public void init(FilterConfig fConfig) throws ServletException {

    }

}
