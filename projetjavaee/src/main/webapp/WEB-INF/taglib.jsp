<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--La diretive taglib -> référencement de bibliothèque de balise externes --%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix ="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- c:import -> Inclure le résultat d'une page HTML ou JSP --%>
<c:import url="header.jsp">
    <%-- paramètre passé au fichier jsp importé --%>
    <c:param name="titre" value="taglib"/>
</c:import>

<h2>JSTL Core</h2>
<h4>Balise d’envoie dans le flux de sortie de la JSP (&lt;c:out&gt;)</h4>
<%-- value -> valeur à afficher et default -> définir une valeur par défaut si la valeur à afficher est null --%>
<p><c:out value="${i}" default="message par défaut"/></p>
<p>${str}</p>
<%-- avec <c:out> par défaut les balises HTML ne sont pas interprétés --%>
<p><c:out value="${str}"/></p>
<%--escapeXml="false" -> autorise l'interprétation des balise HTML --%>
<p><c:out value="${str}" escapeXml="false"/></p>

<h4>Balises de gestion des variables (&lt;c:set&gt; et &lt;c:remove&gt;)</h4>
<%-- Création de la variable v1 --%>
<c:set var="v1" value="100"/>
<p><c:out value="${v1*2}"/></p>
<%-- Destruction de la variable v1 --%>
<c:remove var="v1"/> 
<p><c:out value="${v1}" default="v1 a été supprimé"/></p>
<%-- Affectation d'une valeur àvune variable   --%>
<c:set target="${user}" property="prenom" value="Jane"/>
<c:set target="${user}" property="nom" value="Doe"/>
<p><c:out value="${user.prenom} ${user.nom}"/></p>

<h4>Balises d’opérations conditionnelles (&lt;c:if&gt;)</h4>
<c:if test="${i >100}" var="tstif">
    <p>i est supérieur à 100</p>
</c:if>
<p>test du if= <c:out value="${tstif}"/></p>

<h4>Balises d’opérations conditionnelles (&lt;c:choose&gt;, &lt;c:when&gt; &lt;c:otherwise&gt;)</h4>
<c:choose>
    <c:when test="i>42">
        <p>i est supérieur à 42</p>
    </c:when>
    <c:when test="i<42">
        <p>i est inférieur à 42</p>
    </c:when>
    <c:otherwise>
        <p>i est égal à 42</p>
    </c:otherwise>
</c:choose>


<h4>Balises d’itérations équivalente à un for en java (&lt;c:foreach&gt;)</h4>
<ul>
<c:forEach var="j" begin="5" end="10" step="2">
<li><c:out value="${j}"/></li>
</c:forEach>
</ul>

<h4>Balises d’itérations pour le parcours d'un tableau ou d'une liste (&lt;c:foreach&gt;)</h4>
<ul>
<c:forEach var="elm" items="${t}" begin="1">
<li><c:out value="${elm}"/></li>
</c:forEach>
</ul>

<h4>Attribut varStatus de la balise foreach  pour suivre l’évolution de la boucle</h4> 
<%-- index : indice courant de l’itération
     current : objet courant de l’itération
     count : indique le nombre de passage dans la boucle
     first : true, si c’est la première itération
     last : true, si c’est la dernière itération --%>
     
<c:forEach var="e" items="${t}" varStatus="ind">
    <c:out value="${e} ${ind.index}  ${ind.count}  ${ind.current}  ${ind.first}  ${ind.last}"/> 
    <c:if test="${!ind.last}">
    , 
    </c:if>
</c:forEach>

<p>
<c:forEach var="j" begin="5" end="10" step="2" varStatus="ind">
<c:if test ="${ind.first}">
[
</c:if>
<c:out value="${j}"/>
<c:choose>
<c:when test="${ind.last}">
    ]
</c:when>
<c:otherwise>
    ,
</c:otherwise>
</c:choose>
</c:forEach>
</p>

<h4>Balise de découpe d'une chaine (équivalant d'un split en java) (&lt;c:forTokens&gt;)</h4>
<c:set var="token" value="azerty;yuipo;aaaaa;bbbbbbb;cc;ddd"/>
<c:forTokens items="${token}" var="elm" delims=";" >
<p><c:out value="${elm}"/></p>
</c:forTokens>

<c:forTokens items="${token}" var="elm" delims=";" begin="1" end="4" step="2" >
<p><c:out value="${elm}"/></p>
</c:forTokens>
 <h4>Balises de manipulation d' url  (&lt;c:url&gt;, &lt;c:param&gt;)</h4>
 <!-- context est la racine de l'URL 
  L'url finale est la concaténation de context et value -->
<c:url var="urlpresentation" value="/presentation" context="/projetjavaee"/>
<a class="btn btn-primary" href="${urlpresentation}">Présentation</a>
<a class="btn btn-secondary" href="<c:url value="/presentation" context="/projetjavaee"/>">Présentation</a>

<hr class="my-5"/>
<h2 class="my-3">JSTL Formatage</h2>

<h4>Balise de formatage de valeur numérique(&lt;fmt:formatNumber&gt;)</h4>
<p><fmt:formatNumber type="Number" value="19999023" maxIntegerDigits="6"/></p>
<p><fmt:formatNumber type="Number" value="199" minIntegerDigits="8" /></p>
<p><fmt:formatNumber type="Number" value="199990.23" maxFractionDigits="6"/></p>
<p><fmt:formatNumber type="Number" value="199" minFractionDigits="8" /></p>
<p><fmt:formatNumber type="Number" value="19999.023" pattern="#####.###€"/></p>
<p><fmt:formatNumber type="percent" value="1.0"/></p>
<p><fmt:formatNumber type="currency" value="10" currencySymbol="$"/></p>
<p><fmt:formatNumber type="currency" value="10" currencyCode="JPY"/></p>

<h4>Balise de formatage de date (&lt;fmt:formatDate&gt;, &lt;fmt:setTimeZone&gt;)</h4>
<p><fmt:formatDate value="${date}" dateStyle="short" type="date" /></p>
<p><fmt:formatDate value="${date}" dateStyle="full" type="date" /></p>
<p><fmt:formatDate value="${date}" dateStyle="short" type="time" /></p>
<p><fmt:formatDate value="${date}" dateStyle="long" type="time" /></p>
<p><fmt:formatDate value="${date}" dateStyle="long" type="both" /></p>
<p><fmt:formatDate value="${date}" pattern="dd/MM/yyyy hh:mm" /></p>
<fmt:setTimeZone var="tzGMT" value="GMT" />
<fmt:setTimeZone var="tzParis" value="GMT+2" />
<fmt:setTimeZone var="tzNY" value="GMT-4" />
<p><fmt:formatDate timeZone="${tzGMT}" value="${date}" dateStyle="long" type="both" /></p>
<p><fmt:formatDate timeZone="${tzParis}" value="${date}" dateStyle="long" type="both" /></p>
<p><fmt:formatDate timeZone="${tzNY}" value="${date}" dateStyle="long" type="both" /></p>

<h4>Balise d’internationalisation (&lt;fmt:setBundle&gt;, &lt;fmt:message&gt;</h4>
<%-- <fmt:setLocale value="es"/> --%>
<%-- basename -> nom de base pour les fichiers de propriété ex: inter_en, inter_fr ... --%>
<fmt:setBundle basename="inter" var="inter"/> 
<%-- fmt:message -> fournit la traduction qui correspond à key 
 la langue choisie dépendra de l'en-tête de la requête Accept-Language ou on peut la forcer avec fmt:setLocale --%>
<p class="alert alert-primary">Message= <fmt:message key="bonjour" bundle="${inter}"/></p>

<c:import url="footer.jsp"/>"

