<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="WEB-INF/header.jsp" %>
<div class="container-fluid">
<h1>Exercice Présentation</h1>
<a href="presentation.jsp?name=Alan Smithee">Salutation</a>
<a href="presentation.jsp">J'ignore</a>

<% String name=request.getParameter("name");
   if(name!=null && !name.isEmpty()){%>
   <div class= "alert alert-primary col-md-3">Bonjour, <%=name%></div>
<% } %>   
   
<hr>

<form method="post" action="presentation.jsp">
    <div class="mb-3">
    <label class="col-md-2 form-label" for="prenom">Prénom</label>
    <input class="form-control" "type="text" name="prenom" id="prenom" placeholder="Entrer votre prénom"/>
    </div>
    
    <div class="mb-3">
    <label class="col-md-2 form-label" for="nom">Nom</label>
    <input class="form-control" "type="text" name="nom" id="nom" placeholder="Entrer votre nom"/>
    </div>
    <input class="btn btn-primary mb-3" type="submit" value="confirmer">
</form>
<% String prenom=request.getParameter("prenom");
  String nom=request.getParameter("nom");
   if(nom!=null && !nom.isEmpty() && prenom!=null && !prenom.isEmpty() ){%>
       <div class="alert alert-primary col-md-3">Bonjour, <%= prenom %> <%=nom %></div>
  <%}else {if(nom==null || nom.isEmpty()){%>
  <div class="alert alert-danger col-md-3">Le champs nom est vide</div>
  <% }
   if(prenom==null || prenom.isEmpty()){%>
  <div class="alert alert-danger col-md-3">Le champs prénom est vide</div>
  <%}
  }%>

<%@ include file="WEB-INF/footer.jsp" %>
