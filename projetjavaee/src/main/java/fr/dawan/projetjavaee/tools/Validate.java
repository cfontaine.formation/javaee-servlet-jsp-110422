package fr.dawan.projetjavaee.tools;

public class Validate {

    public static boolean isInteger(String param) {
        return param!=null && !param.isEmpty() &&  param.matches("[+-]?[0-9]+");
    }
    
    public static boolean isNull(String param) {
        return param==null || param.isEmpty();
    }

}
