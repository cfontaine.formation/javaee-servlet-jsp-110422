<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="d" uri="customtag.tld"%>
<c:import url="header.jsp">
    <c:param name="titre" value="exception"/>
</c:import>
<h2 class="mb-5">TagLib personnalisée</h2>
<div class="mb-3">Nous sommes le <d:aujourdhui/></div>
<div class="mb-5">Il est <d:maintenant/></div>
<c:import url="footer.jsp"/>