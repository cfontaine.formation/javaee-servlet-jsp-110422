<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="header.jsp">
    <jsp:param name="titre" value="Langage expression"/>
</jsp:include>
<h2 class="mb-5">Langage expression</h2>

<%-- Accès à un attribut --%>
<p>test= ${test}</p>

<%-- Accès aux propriété d'un objet --%>
<p>User[ ${user1.prenom} , ${user1.nom} ]</p>

<%-- empty -> teste si un objet est null ou vide--%>
<p>user1 est vide= ${empty user1}</p>
<p>user1 n'est pas vide= ${not empty user1}</p>
<p>user2 est vide= ${empty user2}</p>
<p>user3 est vide= ${empty user3}</p>
<%-- Accès à un élément d'un tableau ou d'une liste --%>
<p>t[1]=${t[1]}</p>
<p>t[0] x 2=${t[0]*2}</p>
<p>lst[0]=${lst[0]}</p>
<p>lst[1]=${lst[1]}</p>

<%-- Accès à une valeur stockée dans une map en fonction de la clé-> map[key] --%>
<p>valeur associé à la clé v1= ${m['v1']}</p>

<p>t[2] &gt; 0 et user2 est vide =${t[2] gt 0 and empty user2}</p>

<%@include file="footer.jsp"%>