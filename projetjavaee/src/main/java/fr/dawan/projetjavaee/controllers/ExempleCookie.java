package fr.dawan.projetjavaee.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.dawan.projetjavaee.tools.Validate;

/**
 * Servlet implementation class TestCookie
 */
@WebServlet("/cookie")
public class ExempleCookie extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExempleCookie() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();
        request.setAttribute("cook", cookies);
        request.getRequestDispatcher("WEB-INF/cookies.jsp").forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("name");
        String value = request.getParameter("value");
        String dureeStr = request.getParameter("duree");
        if (!Validate.isNull(name) && !Validate.isNull(value)) {
            int duree = -1;
            if (Validate.isInteger(dureeStr)) {
                duree = Integer.parseInt(dureeStr);
            }
            Cookie c = new Cookie(name, value);
            c.setMaxAge(duree);
            response.addCookie(c);
        }
        response.sendRedirect("/projetjavaee/cookie");
    }

}
