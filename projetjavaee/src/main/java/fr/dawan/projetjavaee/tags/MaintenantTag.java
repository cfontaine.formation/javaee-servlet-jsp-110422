package fr.dawan.projetjavaee.tags;

import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class MaintenantTag extends TagSupport{

    @Override
    public int doStartTag() throws JspException {
        JspWriter out=pageContext.getOut();
        
        try {
            out.print(LocalTime.now().format(DateTimeFormatter.ofPattern("hh:mm:ss")));
        } catch (IOException e) {
            
            e.printStackTrace();
        }
        return SKIP_BODY;
    }
    

}
