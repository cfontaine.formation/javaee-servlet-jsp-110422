package fr.dawan.projetjavaee.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.mindrot.jbcrypt.BCrypt;

import fr.dawan.projetjavaee.beans.Utilisateur;
import fr.dawan.projetjavaee.dao.UtilisateurDao;
import fr.dawan.projetjavaee.tools.Validate;

/**
 * Servlet implementation class Login
 */
@WebServlet("/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String logout=request.getParameter("logout");
		HttpSession session=request.getSession();
		if(!Validate.isNull(logout)) {
		    session.invalidate();
		    request.getRequestDispatcher("index.jsp").forward(request, response);
		}else
		{
		    Utilisateur u= (Utilisateur)session.getAttribute("user");
		    if(u==null) {
		        request.setAttribute("msg", "L'email ou le mot de passe ne sont pas valides");
		        request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
		    }
		    else {
		        response.sendRedirect("articles");
		    }
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email=request.getParameter("email");
		String password=request.getParameter("password");
		UtilisateurDao dao=new UtilisateurDao();
		Utilisateur u=dao.findByMail(email, true);
	//	if(u!=null && password.equals(u.getPassword())) {
		if(u!=null && BCrypt.checkpw(password, u.getPassword())){
		HttpSession session=request.getSession();
		    session.setAttribute("user", u);
		}
		else {
		    request.setAttribute("email", email);
		}
		doGet(request, response);
	}

}
