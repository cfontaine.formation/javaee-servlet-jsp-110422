package fr.dawan.projetjavaee.tags;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class AujourduiTag extends TagSupport{

    @Override
    public int doStartTag() throws JspException {
        JspWriter out=pageContext.getOut();
        
        try {
            out.print(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        } catch (IOException e) {
            
            e.printStackTrace();
        }
        return SKIP_BODY;
    }
    

}
