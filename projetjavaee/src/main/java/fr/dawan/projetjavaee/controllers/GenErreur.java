package fr.dawan.projetjavaee.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.dawan.projetjavaee.tools.Validate;

/**
 * Servlet implementation class GenException
 */
@WebServlet("/generror")
public class GenErreur extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GenErreur() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String choix = request.getParameter("choix");
        System.out.println(choix);
        if (Validate.isNull(choix)) {
            response.sendError(404);
        } else {
            switch (choix) {
            case "jsp":
                request.getRequestDispatcher("WEB-INF/testError.jsp").forward(request, response);
                break;
            case "exception":
                throw new IOException("Test exception");

            case "500":
                response.sendError(500);
                break;
            case "418":
                response.sendError(418);
                break;
            default:
                response.sendError(404);
            }
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }

}
