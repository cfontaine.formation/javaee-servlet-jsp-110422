<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="header.jsp">
    <c:param name="titre" value="Gestion Articles"/>
</c:import>

<c:choose>
<c:when test="${empty article || article.id==0}">
<h3 class="mb-3">Ajouter un article</h3>
</c:when>
<c:otherwise>
<h3 class="mb-3">Modifier un article</h3>
</c:otherwise>
</c:choose>

<div class="mb-3">
<form method="post" action='<c:url value="/articles" context="/projetjavaee" />'>
    <div class="mb-3">
    <label class="col-md-2 form-label" for="nom">Nom</label>
    <input class="form-control" type="text" name="nom" id="nom" placeholder="Entrer le nom du produit" value="${article.nom}"/>
    </div>
    <div class="mb-3">
    <label class="col-md-2 form-label" for="prix">Prix</label>
    <input class="form-control" type="number" min="0" name="prix" id="prix" placeholder="Entrer le prix du produit" value="${article.prix}"/>
    </div> 
    <div class="mb-3">
    <label class="col-md-2 form-label" for="description">Description</label>
    <input class="form-control" type="text" name="description" id="nom" placeholder="Entrer la description du produit" value="${article.description}"/>
    </div>
    <input type="hidden" name="id" value="${article.id}"/>
    <input class="btn btn-primary mb-3 offset-md-5 col-md-2" type="submit" value="Confirmer">
</form>
<c:if test="${!empty msgErr}">
    <div class="alert alert-danger col-md-10 offset-md-1"><c:out value="${msgErr}"/></div>
</c:if>

</div>


<c:import url="footer.jsp"/>