package fr.dawan.projetjavaee.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.dawan.projetjavaee.beans.Article;
import fr.dawan.projetjavaee.dao.fakedao.FakeArticleDao;
import fr.dawan.projetjavaee.tools.Validate;

/**
 * Servlet implementation class ArticlesController
 */
@WebServlet("/articles")
public class ArticlesController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    
    private FakeArticleDao dao=new FakeArticleDao();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ArticlesController() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        String idStr = request.getParameter("id");
        if (action != null) {
            switch (action) {
            case "add":
                request.setAttribute("article", new Article());
                request.getRequestDispatcher("WEB-INF/ajoutArticles.jsp").forward(request, response);
                return;
            case "remove":
                if (Validate.isInteger(idStr)) {
                    dao.deleteById(Integer.parseInt(idStr));
                }
                break;
            case "update":
                if (Validate.isInteger(idStr)) {
                    Article ar=dao.findById(Integer.parseInt(idStr));
                    if(ar!=null) {
                        request.setAttribute("article", ar);
                        request.getRequestDispatcher("WEB-INF/ajoutArticles.jsp").forward(request, response);
                        return;
                    }else
                    {
                        request.setAttribute("msgErr", "L'article n'existe pas");
                    }
                }
                break;
            default:
                request.setAttribute("msgErr", "L'action n'existe pas");
                break;
            }
        }
        List<Article> articles = dao.findAll();
        request.setAttribute("articles", articles);
        request.getRequestDispatcher("WEB-INF/articles.jsp").forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String msgErr=null;
        Article a=new Article();
        // nom
        a.setNom(request.getParameter("nom"));
        if(Validate.isNull(a.getNom())) {
            msgErr="Le nom est vide";
        }
        // description
        a.setDescription(request.getParameter("description"));
        if(Validate.isNull(a.getDescription())) {
            msgErr="La description est vide";
        }
        // prix
        try {
            String prixStr=request.getParameter("prix").trim();
            if(Validate.isNull(prixStr)) {
                msgErr="Le prix est vide";
            }
            else {
                double prix=Double.parseDouble(prixStr);
                a.setPrix(prix);
            }
        }catch(NumberFormatException e) {
            msgErr="Le prix n'est pas un nombre";
        }
        // id
        try {
            String idStr=request.getParameter("id");
            if(Validate.isNull(idStr)) {
                msgErr="L'id produit n'existe pas";
            }
            else {
                int id=Integer.parseInt(idStr);
                a.setId(id);
            }
        }catch(NumberFormatException e) {
            msgErr="L'id n'est pas un nombre";
        }
        if(msgErr==null) {
            dao.saveOrUpdate(a);
            doGet(request,response);
            return;
        }else {
            request.setAttribute("msgErr",msgErr);
            request.setAttribute("article",a);
            request.getRequestDispatcher("WEB-INF/ajoutArticles.jsp").forward(request, response);
        }
    }

}
