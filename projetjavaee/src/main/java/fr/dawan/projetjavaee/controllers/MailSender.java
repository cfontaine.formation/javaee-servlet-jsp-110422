package fr.dawan.projetjavaee.controllers;

import java.io.IOException;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.dawan.projetjavaee.tools.EmailTools;
import fr.dawan.projetjavaee.tools.Validate;

/**
 * Servlet implementation class MainSender
 */
@WebServlet("/email")
public class MailSender extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MailSender() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//FIXME il y a bug
	    request.getRequestDispatcher("WEB-INF/mailsender.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String dest=request.getParameter("dest");
		String exp=request.getParameter("exp");
		String titre=request.getParameter("titre");
		String content=request.getParameter("content");
		if(!Validate.isNull(content) && !Validate.isNull(titre) && !Validate.isNull(dest) &&!Validate.isNull(exp)) {
		    String messageHtml="<html> <body><h1>Formation JavaEE</h1><hr/><p>"+content +"</p></body></html>"; 
		    Properties prop=new Properties();
		    // Configuration pour fakeSMTP
		    prop.put("mail.smtp.host", "localhost");
		    prop.put("mail.smtp.port", "250");
		    EmailTools email=new EmailTools(prop);
		    try {
                email.sendHtmlEmail(exp, dest, titre, messageHtml);
            }  catch (MessagingException e) {
                e.printStackTrace();
            }
		}
		doGet(request, response);
	}

}
