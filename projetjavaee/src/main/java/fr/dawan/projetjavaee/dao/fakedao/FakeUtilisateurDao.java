package fr.dawan.projetjavaee.dao.fakedao;

import java.util.ArrayList;
import java.util.List;

import fr.dawan.projetjavaee.beans.Utilisateur;

public class FakeUtilisateurDao {

    private static List<Utilisateur> lst = new ArrayList<>();

    // Un bloc static est éxécuté une seule fois lors du premier objet instancier
    // avant l'éxécution du constructeur
    static {
        Utilisateur u = new Utilisateur("John", "Doe", "jodoe@dawan.com", "");
        u.setId(1);
        lst.add(u);
        u = new Utilisateur("Jane", "Doe", "jadoe@dawan.com", "");
        u.setId(2);
        lst.add(u);
        u = new Utilisateur("Allan", "Smithee", "alsmithee@dawan.com", "");
        u.setId(3);
        lst.add(u);
        u = new Utilisateur("George", "Spelvin", "gspelvin@dawan.com", "");
        u.setId(4);
        lst.add(u);
        u = new Utilisateur("Karen", "Eliot", "keliotdawan.com", "");
        u.setId(5);
        lst.add(u);
    }

    public void saveOrUpdate(Utilisateur u) {
        if (u.getId() == 0) {
            create(u);
        } else {
            update(u);
        }
    }

    public void delete(Utilisateur u) {
        lst.remove(u);
    }

    public void deleteById(long id) {
        for (Utilisateur ur : lst) {
            if (ur.getId() == id) {
                lst.remove(ur);
                break;
            }
        }
    }

    public List<Utilisateur> findAll() {
        return lst;
    }

    public Utilisateur findById(long id) {
        for (Utilisateur ur : lst) {
            if (ur.getId() == id) {
                return ur;
            }
        }
        return null;
    }

    private void create(Utilisateur u) {
        lst.add(u);
        u.setId(lst.size() + 1);
    }

    private void update(Utilisateur u) {
        for (Utilisateur ur : lst) {
            if (ur.getId() == u.getId()) {
                ur.setPrenom(ur.getPrenom());
                ur.setNom(ur.getNom());
                ur.setEmail(ur.getEmail());
                ur.setPassword(ur.getPassword());
                break;
            }
        }
    }
}
