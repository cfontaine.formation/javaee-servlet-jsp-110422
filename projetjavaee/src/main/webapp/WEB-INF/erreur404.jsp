<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isErrorPage="true"%>
<%@ include file="header.jsp"%>
<h1>Erreur 404 (web.xml)</h1>
<div class="display-1 text-center">Erreur <c:out value="${pageContext.errorData.statusCode}"/></div>
<div class="display-4 text-center"><c:out value="${pageContext.errorData.requestURI}"/></div>
<div class="display-4 text-center"><c:out value="${pageContext.errorData.servletName}"/></div>
<%@ include file="footer.jsp"%>